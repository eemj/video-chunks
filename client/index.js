;(function () {
  const videoContainer = document.getElementById('videoContainer')

  let videoIndex = 0
  let videoSize = 0

  async function main () {
    await fetch('/videos')
      .then(res => res.json())
      .then(data => {
        videoSize = data[videoIndex].size
      })
      .catch(err => {
        console.error(err)
      })

    let bufferSize = Math.floor(videoSize / (8 * 1024))

    console.log(`Buffer Size: ${bufferSize}`)
    console.log(`Video Size: ${videoSize}`)

    const video = document.createElement('video')
    video.autoplay = true
    video.controls = true

    let videoBuffer = new Uint8Array(videoSize)

    await Promise.all(
      Array.from(Array(Math.ceil(videoSize / bufferSize)).keys()).map(i =>
        fetch(`/videos/${videoIndex + 1}?size=${bufferSize}&after=${bufferSize * i}`)
          .then(res => res.body.getReader())
          .then(reader => {
            let buffer = []

            return new Promise(resolve => {
              function push () {
                reader.read().then(({ done, value }) => {
                  if (done) {
                    resolve(buffer)
                    return
                  }

                  buffer = buffer.concat(Array.from(value))

                  push()
                })
              }

              push()
            })
          })
          .then(buffer => {
            for (let j = 0; j < bufferSize; j++) {
              videoBuffer[j + bufferSize * i] = buffer[j]
            }
          })
          .catch(err => {
            console.error(err)
          })
      )
    )

    const blob = new Blob([videoBuffer], { type: 'application/octet-stream' })
    video.src = URL.createObjectURL(blob)

    videoContainer.appendChild(video)
  }

  main().catch(err => {
    console.error(err)
  })
})()
