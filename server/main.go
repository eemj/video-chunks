package main

import (
	"context"
	"encoding/json"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"sync"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
)

const (
	videosDir string = "./videos"
	clientDir string = "./client"
)

type contextKey string

type File struct {
	sync.Mutex

	Size int64  `json:"size"`
	ID   int    `json:"id"`
	Path string `json:"-"`
	Name string `json:"name"`
}

func getFiles() (*[]File, error) {
	files, err := ioutil.ReadDir(videosDir)

	if err != nil {
		return nil, err
	}

	outputFiles := &[]File{}

	if len(files) > 0 {
		var offset int = 0
		for _, f := range files {
			if f.Name()[0] != '.' {
				offset += 1
				(*outputFiles) = append(*outputFiles, File{
					ID:   offset,
					Size: f.Size(),
					Name: f.Name(),
					Path: filepath.Join(videosDir, f.Name()),
				})
			}
		}
	}

	return outputFiles, nil
}

func getBytes(file File, after, bufferSize int64) (*[]byte, error) {
	file.Lock()
	defer file.Unlock()

	fs, err := os.Open(file.Path)

	if err != nil {
		return nil, err
	}

	buf := make([]byte, bufferSize)

	p, err := fs.ReadAt(buf, after)

	switch err {
	case io.EOF:
		sliced := buf[:p]
		return &sliced, nil
	case nil:
		return &buf, nil
	default:
		return nil, err
	}
}

func main() {
	router := chi.NewRouter()

	router.Use(middleware.Logger)

	router.Mount("/", http.StripPrefix("/", http.FileServer(http.Dir(clientDir+"/"))))

	router.Route("/videos", func(r chi.Router) {
		r.Use(func(f http.Handler) http.Handler {
			return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				files, err := getFiles()

				if err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}

				ctx := context.WithValue(r.Context(), contextKey("files"), *files)

				f.ServeHTTP(w, r.WithContext(ctx))
			})
		})

		r.Get("/", func(w http.ResponseWriter, r *http.Request) {
			files := r.Context().Value(contextKey("files")).([]File)

			payload, err := json.Marshal(files)

			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusAccepted)
			w.Write(payload)
		})

		r.Get("/{id:\\d+}", func(w http.ResponseWriter, r *http.Request) {
			id, _ := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)

			after := r.URL.Query().Get("after")
			var afterValue int64 = 0
			if len(after) > 0 {
				val, err := strconv.ParseInt(after, 10, 64)
				if err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}
				afterValue = val
			}

			bufferSize := r.URL.Query().Get("size")
			var size int64 = 10240
			if len(bufferSize) > 0 {
				val, err := strconv.ParseInt(bufferSize, 10, 64)
				if err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}
				size = val
			}

			files := r.Context().Value(contextKey("files")).([]File)

			file := new(File)
			for _, f := range files {
				if int64(f.ID) == id {
					*file = f
					break
				}
			}

			if file == nil {
				http.Error(w, "cannto find file", http.StatusNotFound)
				return
			}

			content, err := getBytes(*file, afterValue, size)

			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			w.Header().Set("Content-Length", strconv.Itoa(len(*content)))
			w.Header().Set("X-Content-Length", strconv.Itoa(len(*content)))
			w.Header().Set("Content-Type", "application/octet-stream")
			w.Write(*content)
			w.WriteHeader(http.StatusOK)
		})
	})

	log.Fatal(http.ListenAndServe(":8080", router))
}
